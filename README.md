# csm-thesis-minimal

This is a barebones LaTeX template for Colorado School of Mines.

## Caveat Emptor

This template is provided ﻿“AS IS” without warranty of any kind, either
expressed or implied, including, but not limited to, the implied warranties
of merchantability and fitness for a particular purpose.  The entire risk as
to the quality and performance of the template is with you.  Should the
template prove defective, you assume the cost of all necessary servicing,
repair or correction. That said, the Colorado School of Mines writing center
can likely help with formatting.

## Philosophy

Although this template uses parts of the unofficial thesis template at
http://www.compholio.com/csm/csm-thesis/, this template tries to be as
simple as possible (KISS) while still following the rules outlined at
https://www.mines.edu/graduate-studies/thesis-writers-guide/.

It is supposed to be simpler, easier to modify, and reliant on more external packages.
The number of custom arcane LaTeX commands has been cut down as much as possible.
However because this is LaTeX, there are always some caveats.

* The package `hyperref` will likely cause problems if you try and use it, since it
  conflicts with the titling commands.
* Landscaping will likely require fine tuning to get the page numbers correct (assuming you
  are using pdflscape.
* Figures and Tables might need some fine tuning, but this is the case everywhere.

Since every use-case is different, I have tried not to prematurely solve any of these problems,
in case the behavior is not what you expect.

## How to use

Since https://www.mines.edu/graduate-studies/thesis-writers-guide/ requires Times New Roman,
you have to compile with XeLaTeX and have the font installed. You also need to use bibtex
to compile your citations (this can be changed in the template and is not hard coded).

So to fully compile you have to run (if using linux of macos)

```
xelatex example.tex
bibtex example
xelatex example.tex
xelatex example.tex
```

You can set similar settings in most modern LaTeX IDEs.

## How to modify

For most changes, you can simply add your packages and settings at the top.
For change to titles, table of contents, or general document style, you might
have to modify `csm-thesis.cls`.
